/*
 * @Descripttion:
 * @version:
 * @Author: MichaelHu
 * @Date: 2020-09-22 13:23:35
 * @LastEditors: MichaelHu
 * @LastEditTime: 2022-11-24 00:08:28
 */
#include "usart.h"
#include "FIFO.h"

FIFOTYPE *uartFIFO;
static u16 uart_timeout = 0;
static u8 read_Flag = 0; //用于接收数据超时判断
static u32 data_len = 0; //数据长度
bool uartFlag = false;
u8 *process_buf;

/* Suport printf function, useMicroLib is unnecessary */
#ifdef __CC_ARM
#pragma import(__use_no_semihosting)
struct __FILE
{
    int handle;
};

FILE __stdout;

void _sys_exit(int x)
{
    x = x;
}

void _ttywrch(int ch)
{
    ch = ch;
}

#endif

#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
     set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */

/**
 * @brief  Retargets the C library printf function to the USART.
 * @param  None
 * @retval None
 */
PUTCHAR_PROTOTYPE
{
#ifdef DEBUG
    USART_SendData(USART1, ch);
    while (USART_GetFlagStatus(USART1, USART_FLAG_TRAC) == RESET) {}
#endif
    return ch;
}

void uart_init(u32 bound)
{
    GPIO_InitType GPIO_InitStructure;
    USART_InitType USART_InitStructure;

    /* Enable GPIO clock */
    RCC_AHBPeriphClockCmd(RCC_AHBPERIPH_GPIOA, ENABLE);

    /* Enable UART clock */
    RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_USART1, ENABLE);

    /* Connect PXx to USART1_Tx */
    GPIO_PinAFConfig(GPIOA, GPIO_PinsSource9, GPIO_AF_1);
    /* Connect PXx to USART1_Rx */
    GPIO_PinAFConfig(GPIOA, GPIO_PinsSource10, GPIO_AF_1);

    /* Configure USART1 Tx/Rx */
    GPIO_InitStructure.GPIO_Pins = GPIO_Pins_9 | GPIO_Pins_10;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
    GPIO_InitStructure.GPIO_OutType = GPIO_OutType_PP;
    GPIO_InitStructure.GPIO_Pull = GPIO_Pull_NOPULL;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    // USART 初始化设置
    USART_StructInit(&USART_InitStructure);
    USART_InitStructure.USART_BaudRate = bound;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
    USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

    /* USART configuration */
    USART_Init(USART1, &USART_InitStructure);

    /* Enable USART */
    USART_Cmd(USART1, ENABLE); //使能串口1

#ifndef FIFO_DMA
    NVIC_InitType NVIC_InitStructure;
    // NVIC 配置
    NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    USART_INTConfig(USART1, USART_INT_IDLEF, ENABLE); //开启串口接受中断
#endif
}

#ifndef FIFO_DMA
void USART1_IRQHandler(void) //串口1中断服务程序
{
    if (USART_GetITStatus(USART1, USART_IT_RXNE) != RESET) //接收到数据
    {
        uartFIFO->buffer[FIFO_BUF_LENGTH - uartFIFO->CNDTR] = USART_ReceiveData(USART1); //读取接收到的数据
        uartFIFO->CNDTR--;
        if (uartFIFO->CNDTR == 0)
        {
            uartFIFO->CNDTR = FIFO_BUF_LENGTH;
        }
    }
}
#endif

void MYDMA_Config(DMA_Channel_Type *DMA_CHx, u32 cpar, u32 cmar, u16 cndtr)
{
    DMA_InitType DMA_InitStructure;

    /* DMA clock enable */
    RCC_AHBPeriphClockCmd(RCC_AHBPERIPH_DMA1, ENABLE);

    /* USART1_Rx_DMA_Channel (triggered by USART1 Tx event) Config */
    DMA_Reset(DMA_CHx);
    DMA_DefaultInitParaConfig(&DMA_InitStructure);
    DMA_InitStructure.DMA_PeripheralBaseAddr = cpar;         //外设地址
    DMA_InitStructure.DMA_MemoryBaseAddr = cmar;             //内存地址
    DMA_InitStructure.DMA_Direction = DMA_DIR_PERIPHERALSRC; //传输方向外设到内存
    DMA_InitStructure.DMA_BufferSize = cndtr;                //传输量
    DMA_InitStructure.DMA_PeripheralInc = DMA_PERIPHERALINC_DISABLE;
    DMA_InitStructure.DMA_MemoryInc = DMA_MEMORYINC_ENABLE;
    DMA_InitStructure.DMA_PeripheralDataWidth = DMA_PERIPHERALDATAWIDTH_BYTE; //外设地址不自增
    DMA_InitStructure.DMA_MemoryDataWidth = DMA_MEMORYDATAWIDTH_BYTE;         //内存地址自增
    DMA_InitStructure.DMA_Mode = DMA_MODE_CIRCULAR;                           //循环模式
    DMA_InitStructure.DMA_Priority = DMA_PRIORITY_VERYHIGH;                   //高优先级
    DMA_InitStructure.DMA_MTOM = DMA_MEMTOMEM_DISABLE;
    DMA_Init(DMA_CHx, &DMA_InitStructure);

    /* Enable USART1 DMA TX request */
    USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);

    DMA_ChannelEnable(DMA_CHx, ENABLE);
}

static void TIM3_Int_Init(u16 arr, u16 psc)
{
    TMR_TimerBaseInitType TIM_TimeBaseInitStructure;
    NVIC_InitType NVIC_InitStructure;
    RCC_APB1PeriphClockCmd(RCC_APB1PERIPH_TMR3, ENABLE);

    TIM_TimeBaseInitStructure.TMR_DIV = psc; //分频值
    TIM_TimeBaseInitStructure.TMR_CounterMode = TMR_CounterDIR_Up;
    TIM_TimeBaseInitStructure.TMR_Period = arr; //自动重装数值
    TIM_TimeBaseInitStructure.TMR_ClockDivision = TMR_CKD_DIV1;
    TMR_TimeBaseInit(TMR3, &TIM_TimeBaseInitStructure);
    TMR_INTConfig(TMR3, TMR_INT_Overflow, ENABLE);

    NVIC_InitStructure.NVIC_IRQChannel = TMR3_GLOBAL_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

    TMR_Cmd(TMR3, ENABLE);
}

void time_process_callback()
{
    uart_timeout++;
    if (read_Flag) //超过时间还没有收到数据，则清零标志位
    {
        //波特率460800＝ 460800 (位/秒) ＝ 46.08 (KB/秒)， 最大接收数据长度 = 5ms*46.08(B/ms)= 230.4
        //目前测试50ms时间，可以接收处理2000字节数据，根据字节需要修改时间
        if ((data_len > FIFO_Status(uartFIFO, uartCNDTR)) && (uart_timeout >= 500))
        {
            read_Flag = 0;
        }
    }
}

void TMR3_GLOBAL_IRQHandler(void)
{
    if (TMR_GetINTStatus(TMR3, TMR_INT_Overflow) != RESET)
    {
        TMR_ClearITPendingBit(TMR3, TMR_INT_Overflow);
        time_process_callback();
        
    }
}

void usart_dma_init(u32 bound)
{
    uart_init(bound);
    //初始化FIFO
    FIFO_Init(&uartFIFO, FIFO_BUF_LENGTH);
    TIM3_Int_Init(1000 - 1, 72 - 1); // 1MHZ 1000次  1ms计时

    MYDMA_Config(uartDMA_CH, (u32)&USART1->DT, uartFIFO->staraddr, uartFIFO->size);
}

void send_Serial_CMD(USART_Type *USARTx, u8 cmd)
{
    u16 len = 0;
    u32 crc;
    u8 send_buf[200];

    send_buf[len++] = 0x55;
    send_buf[len++] = 0x55;
    send_buf[len++] = 0;   //长度高位
    send_buf[len++] = 0;   //长度低位
    send_buf[len++] = cmd; // CMD

    send_buf[2] = (u8)(len >> 8);
    send_buf[3] = (u8)(len >> 0);

    crc = 0;
    crc = calc_crc32(crc, &send_buf[4], len - 4);

    send_buf[len++] = (u8)(crc >> 24);
    send_buf[len++] = (u8)(crc >> 16);
    send_buf[len++] = (u8)(crc >> 8);
    send_buf[len++] = (u8)(crc >> 0);

    for (int i = 0; i < len; i++)
    {
        while ((USARTx->STS & 0X40) == 0) {};
        USARTx->DT = (u8)send_buf[i];
    }
}

/**
 * @description: 获取数据
 * @param {FIFOTYPE} *fifo 数据指针
 * @param {u8} *cmd 指令
 * @param {u32} **data 数据
 * @param {u32} ** secondData 数据循环接收完成后的环形接收头数据
 * @param {u16} *secondLen	第二段数据长度
 * @param {u16} *totalLen	总数据长度
 * @return {*}
 */
Process_RES_TypeDef data_Process(FIFOTYPE *fifo, u8 *cmd, u32 **data, u32 **secondData, u16 *secondLen, u16 *totalLen)
{
    u8 read_Data;
    u8 lenH, lenL;
    u32 crc;
    u8 crc_buf[4];
    static u8 processFlag = false;
    static u32 read_len;
    if (read_Flag == 1) //收到头码，等待数据接收完成，超时则清零
    {
        read_len = FIFO_Status(fifo, uartCNDTR);
        if (read_len >= data_len)
        {
            processFlag = 1;
        }
    }
    else
    {
        /*接收头码*/
        if (FIFO_Status(fifo, uartCNDTR) >= 6) //最小一帧数据6字节
        {
            read_Data = 0;
            FIFO_Read(fifo, &read_Data, 1, uartCNDTR);
            if (read_Data != 0x55)
            {
                return NG;
            }
            else
            {
                read_Data = 0;
                FIFO_Read(fifo, &read_Data, 1, uartCNDTR);
                if (read_Data != 0x55)
                {
                    return NG;
                }
                else
                {
                    //读取到头码
                    data_len = 0;
                    FIFO_Read(fifo, &lenH, 1, uartCNDTR);
                    FIFO_Read(fifo, &lenL, 1, uartCNDTR);
                    data_len = (lenH << 8) + lenL;

                    if ((data_len + 4) > FIFO_BUF_LENGTH) //数据长度大于整个BUF的大小则返回错误
                    {
                        return BUF_LENGTH_ERR;
                    }

                    if (FIFO_Status(fifo, uartCNDTR) >= data_len)
                    {
                        processFlag = 1;
                    }
                    else
                    {
                        uart_timeout = 0;
                        read_Flag = 1; //数据没有接收完成，标志置位，等待定时器中判断
                        return NG;
                    }
                }
            }
        }
    }
    if (processFlag)
    {
        processFlag = false;
        /*处理数据*/
        FIFO_Read(fifo, cmd, 1, uartCNDTR);
        *totalLen = data_len - 5;
        read_Flag = 0; //清除标志

        u32 surplus;
        surplus = ((u32)(fifo->endaddr) + 1 - (u32)(fifo->front));
        if (surplus < *totalLen)
        {
            *data = ((u32 *)(fifo->front));
            *secondLen = *totalLen - surplus;
            *secondData = ((u32 *)(fifo->staraddr));
        }
        else
        {
            *secondLen = 0;
            *data = ((u32 *)(fifo->front));
        }

        crc = 0;
        crc = calc_crc32(crc, cmd, 1);
        crc = calc_crc32(crc, ((u8 *)(fifo->front)), *totalLen - *secondLen);
        crc = calc_crc32(crc, ((u8 *)(fifo->staraddr)), *secondLen);

        FIFO_Remove(fifo, data_len - 1 - 4);
        if (FIFO_Status(fifo, uartCNDTR) >= 4)
        {
            //读取CRC
            FIFO_ReadN(fifo, crc_buf, 4, uartCNDTR);
        }
        u32 c = (u32)crc_buf[0] << 24 | (u32)crc_buf[1] << 16 | (u32)crc_buf[2] << 8 | (u32)crc_buf[3] << 0;
        if (crc == c)
        {
            return OK;
        }
        else
        {
            return CRC_NG;
        }
    }
    return NG;
}

Process_RES_TypeDef data_Process_onebuf(FIFOTYPE *fifo, u8 *cmd, u8 *data, u16 *len)
{
    u8 res;
    u8 *buf;
    u8 *buf1;
    u16 secondLen;
    u16 Tlen;

    res = data_Process(fifo, cmd, (u32 **)&buf, (u32 **)&buf1, &secondLen, &Tlen);
    if (res == OK)
    {
        *len = Tlen;
        if (secondLen > 0)
        {
            u16 cnt = 0;
            for (int i = 0; i < (Tlen - secondLen); i++)
            {
                data[cnt++] = buf[i];
            }
            for (int i = 0; i < secondLen; i++)
            {
                data[cnt++] = buf1[i];
            }
        }
        else
        {
            for (int i = 0; i < Tlen; i++)
            {
                data[i] = buf[i];
            }
        }
    }
    return res;
}
