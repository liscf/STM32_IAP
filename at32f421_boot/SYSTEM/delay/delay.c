#include "delay.h"
#include "FreeRTOS.h"
#include "task.h"

/*delay macros*/
#define STEP_DELAY_MS 500

/*delay variable*/
static __IO u32 fac_us;
static __IO u16 fac_ms;

extern void xPortSysTickHandler(void);

void SysTick_Handler(void)
{
    if (xTaskGetSchedulerState() != taskSCHEDULER_NOT_STARTED) //系统已经运行
    {
        xPortSysTickHandler();
    }
}

/**
  * @brief  initialize Delay function   
  * @param  None
  * @retval None
  */
void delay_init(u8 SYSCLK)
{
    u32 reload;
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK); //SysTick频率为HCLK
    fac_us = SYSCLK;                                 //不论是否使用OS,fac_us都需要使用
    reload = SYSCLK;                                 //每秒钟的计数次数 单位为K
    reload *= 1000000 / configTICK_RATE_HZ;          //根据configTICK_RATE_HZ设定溢出时间
                                                     //reload为24位寄存器,最大值:16777216,在180M下,约合0.745s左右
    fac_ms = 1000 / configTICK_RATE_HZ;              //代表OS可以延时的最少单位
    SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;       //开启SYSTICK中断
    SysTick->LOAD = reload;                          //每1/configTICK_RATE_HZ断一次
    SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;        //开启SYSTICK
}

//延时nus
//nus:要延时的us数.
//nus:0~190887435(最大值即2^32/fac_us@fac_us=22.5)
void delay_us(u32 nus)
{
    u32 ticks;
    u32 told, tnow, tcnt = 0;
    u32 reload = SysTick->LOAD; //LOAD的值
    ticks = nus * fac_us;       //需要的节拍数
    told = SysTick->VAL;        //刚进入时的计数器值
    while (1)
    {
        tnow = SysTick->VAL;
        if (tnow != told)
        {
            if (tnow < told)
                tcnt += told - tnow; //这里注意一下SYSTICK是一个递减的计数器就可以了.
            else
                tcnt += reload - tnow + told;
            told = tnow;
            if (tcnt >= ticks)
                break; //时间超过/等于要延迟的时间,则退出.
        }
    };
}

//延时nms,会引起任务调度
//nms:要延时的ms数
//nms:0~65535
void delay_ms(u32 nms)
{
    if (xTaskGetSchedulerState() != taskSCHEDULER_NOT_STARTED) //系统已经运行
    {
        if (nms >= fac_ms) //延时的时间大于OS的最少时间周期
        {
            vTaskDelay(nms / fac_ms); //FreeRTOS延时
        }
        nms %= fac_ms; //OS已经无法提供这么小的延时了,采用普通方式延时
    }
    delay_us((u32)(nms * 1000)); //普通方式延时
}

//延时nms,不会引起任务调度
//nms:要延时的ms数
void delay_xms(u32 nms)
{
    u32 i;
    for (i = 0; i < nms; i++) delay_us(1000);
}

/**
  * @brief  Inserts a delay time.
  * @param  sec: specifies the delay time length, in seconds.
  * @retval None
  */
void delay_sec(u16 sec)
{
    u16 i;
    for (i = 0; i < sec; i++)
    {
        delay_ms(500);
        delay_ms(500);
    }
}
