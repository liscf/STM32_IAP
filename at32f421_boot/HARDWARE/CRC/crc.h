/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2022-11-14 20:36:48
 * @LastEditors: MichaelHu
 * @LastEditTime: 2022-11-14 20:37:12
 */
#ifndef __CRC32_H_
#define __CRC32_H_
#include "sys.h"

unsigned int calc_crc32(unsigned int crc, const void *buf, unsigned int size);
unsigned char CRC8(unsigned char buffer[], int startlength, int length);
	
#endif
