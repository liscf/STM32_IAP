/*
 * @Descripttion:
 * @version:
 * @Author: MichaelHu
 * @Date: 2022-11-14 11:11:10
 * @LastEditors: MichaelHu
 * @LastEditTime: 2022-11-24 09:25:59
 */
#include "st7789.h"
#include "FreeRTOS.h"
#include "delay.h"
#include "font.h"
#include "sfud.h"
#include "sys.h"

u16 POINT_COLOR; //默认画笔颜色
u16 BACK_COLOR;  //默认背景颜色

void lcd_io_init(GPIO_Type *GPIOx, uint16_t GPIO_Pin)
{
    GPIO_InitType GPIO_InitStructure;
    GPIO_InitStructure.GPIO_Pins = GPIO_Pin;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
    GPIO_Init(GPIOx, &GPIO_InitStructure);
}

void screen_gpio_init(void)
{
#ifdef HARDWARE_SPI
    GPIO_InitType GPIO_InitStructure;
    SPI_InitType SPI_InitStructure;

    RCC_AHBPeriphClockCmd(RCC_AHBPERIPH_GPIOA | RCC_AHBPERIPH_GPIOB, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2PERIPH_SPI1, ENABLE);

    GPIO_InitStructure.GPIO_Pins = GPIO_Pins_5 | GPIO_Pins_6 | GPIO_Pins_7;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStructure.GPIO_MaxSpeed = GPIO_MaxSpeed_50MHz;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

    GPIO_PinAFConfig(GPIOA, GPIO_PinsSource5, GPIO_AF_0);
    GPIO_PinAFConfig(GPIOA, GPIO_PinsSource6, GPIO_AF_0);
    GPIO_PinAFConfig(GPIOA, GPIO_PinsSource7, GPIO_AF_0);

    /* SPI configuration ------------------------------------------------------*/
    SPI_DefaultInitParaConfig(&SPI_InitStructure);
    SPI_InitStructure.SPI_TransMode = SPI_TRANSMODE_FULLDUPLEX;
    SPI_InitStructure.SPI_Mode = SPI_MODE_MASTER;
    SPI_InitStructure.SPI_FrameSize = SPI_FRAMESIZE_8BIT;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_LOW;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_1EDGE;
    SPI_InitStructure.SPI_NSSSEL = SPI_NSSSEL_SOFT;
    SPI_InitStructure.SPI_MCLKP = SPI_MCLKP_2;
    SPI_InitStructure.SPI_FirstBit = SPI_FIRSTBIT_MSB;
    SPI_InitStructure.SPI_CPOLY = 7;
    SPI_Init(SPI1, &SPI_InitStructure);
    /* Enable SPI1 */
    SPI_Enable(SPI1, ENABLE);
#else
    RCC_AHBPeriphClockCmd(RCC_AHBPERIPH_GPIOA | RCC_AHBPERIPH_GPIOB, ENABLE);
    lcd_io_init(SCL_PORT);
    lcd_io_init(SDA_PORT);
#endif
    lcd_io_init(DC_PORT);
    lcd_io_init(CS_PORT);
    lcd_io_init(RST_PORT);
    lcd_io_init(LCD_LED);
}

void SPI_DMA(u32 sendBuf, u32 bufSize)
{
    DMA_InitType DMA_InitStructure;
    /* SPI_SLAVE_Rx_DMA_Channel configuration ---------------------------------------------*/
    DMA_Reset(DMA1_Channel3);
    DMA_DefaultInitParaConfig(&DMA_InitStructure);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) & (SPI1->DT);
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)sendBuf;
    DMA_InitStructure.DMA_Direction = DMA_DIR_PERIPHERALDST;
    DMA_InitStructure.DMA_BufferSize = bufSize;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PERIPHERALINC_DISABLE;
    DMA_InitStructure.DMA_MemoryInc = DMA_MEMORYINC_ENABLE;                   //内存地址自增
    DMA_InitStructure.DMA_PeripheralDataWidth = DMA_PERIPHERALDATAWIDTH_BYTE; //外设数据单位
    DMA_InitStructure.DMA_MemoryDataWidth = DMA_MEMORYDATAWIDTH_BYTE;         //内存数据单位
    DMA_InitStructure.DMA_Mode = DMA_MODE_NORMAL;                             //单传传输
    DMA_InitStructure.DMA_Priority = DMA_PRIORITY_LOW;                        //优先级
    DMA_InitStructure.DMA_MTOM = DMA_MEMTOMEM_DISABLE;                        //禁止内存到内存
    DMA_Init(DMA1_Channel3, &DMA_InitStructure);

    /* Enable SPI_SLAVE Rx request */
    // SPI_I2S_DMAEnable(SPI1, SPI_I2S_DMA_TX, ENABLE);

    /* Enable DMA1 Channel4 */
    DMA_ChannelEnable(DMA1_Channel3, ENABLE);
}

void SPI_DMA_BufferCounter_Reset(u32 size)
{
    DMA_ChannelEnable(DMA1_Channel3, DISABLE);
    DMA_SetCurrDataCounter(DMA1_Channel3, size);
    DMA_ChannelEnable(DMA1_Channel3, ENABLE);
}

#ifdef HARDWARE_SPI

u8 LCD_SPI_Send(unsigned char dat)
{
    u16 retry = 0;
    while ((SPI1->STS & 1 << 1) == 0) //等待发送区空
    {
        retry++;
        if (retry >= 200)
            return 0; //超时退出
    }
    SPI1->DT = dat; //发送一个byte
    retry = 0;
    while ((SPI1->STS & 1 << 0) == 0) //等待接收完一个byte
    {
        retry++;
        if (retry >= 200)
            return 0; //超时退出
    }
    return SPI1->DT; //返回收到的数据
}
#else

void LCD_SPI_Send(unsigned char dat)
{
    for (int i = 0; i < 8; i++)
    {
        SCLK_L;
        if ((dat & 0x80) != 0)
        {
            SDA_H;
        }
        else
        {
            SDA_L;
        }

        dat <<= 1;
        SCLK_H;
    }
}
#endif

static void LCD_Write_Cmd(unsigned int i)
{
    CS_L;
    DC_L;
    LCD_SPI_Send(i);

    CS_H;
}

static void LCD_WriteData8(unsigned int i)
{
    CS_L;
    DC_H;

    LCD_SPI_Send(i);

    CS_H;
}

void LCD_WriteData16(const u16 da)
{
    CS_L;
    DC_H;
    LCD_SPI_Send(da >> 8);
    LCD_SPI_Send(da);
    CS_H;
}

void LCD_WriteData(u8 *data, u32 len)
{
    CS_L;
    DC_H;
    for (int i = 0; i < len; i++)
    {
        LCD_SPI_Send(data[i]);
    }
    CS_H;
}

void LCD_DisplayOn(void)
{
    LCD_LED_ON;
}

void LCD_DisplayOff(void)
{
    LCD_LED_OFF;
}

void LCD_Init(void)
{
    screen_gpio_init();

    RST_L;
    delay_ms(100);

    RST_H;

    /* Sleep Out */
    LCD_Write_Cmd(0x11);
    /* wait for power stability */
    delay_ms(120);

    /* Memory Data Access Control */
    LCD_Write_Cmd(0x36);
    if (USE_HORIZONTAL == 0)
        LCD_WriteData8(0x00);
    else if (USE_HORIZONTAL == 1)
        LCD_WriteData8(0xC0);
    else if (USE_HORIZONTAL == 2)
        LCD_WriteData8(0x70);
    else
        LCD_WriteData8(0xA0);

    /* RGB 5-6-5-bit  */
    LCD_Write_Cmd(0x3A);
    LCD_WriteData8(0x65);

    /* Porch Setting */
    LCD_Write_Cmd(0xB2);
    LCD_WriteData8(0x0C);
    LCD_WriteData8(0x0C);
    LCD_WriteData8(0x00);
    LCD_WriteData8(0x33);
    LCD_WriteData8(0x33);

    /*  Gate Control */
    LCD_Write_Cmd(0xB7);
    LCD_WriteData8(0x71);

    /* VCOM Setting */
    LCD_Write_Cmd(0xBB);
    LCD_WriteData8(0x35); // Vcom=1.625V

    /* LCM Control */
    LCD_Write_Cmd(0xC0);
    LCD_WriteData8(0x2C);

    /* VDV and VRH Command Enable */
    LCD_Write_Cmd(0xC2);
    LCD_WriteData8(0x01);
    LCD_WriteData8(0xFF);
    /* VRH Set */
    LCD_Write_Cmd(0xC3);
    LCD_WriteData8(0x15);

    /* VDV Set */
    LCD_Write_Cmd(0xC4);
    LCD_WriteData8(0x20);

    /* Frame Rate Control in Normal Mode */
    LCD_Write_Cmd(0xC6);
    LCD_WriteData8(0x0F); // 60MHZ

    /* Power Control 1 */
    LCD_Write_Cmd(0xD0);
    LCD_WriteData8(0xA4);
    LCD_WriteData8(0xA1);

    /* Positive Voltage Gamma Control */
    LCD_Write_Cmd(0xE0);
    LCD_WriteData8(0xD0);
    LCD_WriteData8(0x0A);
    LCD_WriteData8(0x0E);
    LCD_WriteData8(0x0B);
    LCD_WriteData8(0x09);
    LCD_WriteData8(0x24);
    LCD_WriteData8(0x2A);
    LCD_WriteData8(0x3B);
    LCD_WriteData8(0x40);
    LCD_WriteData8(0x28);
    LCD_WriteData8(0x13);
    LCD_WriteData8(0x13);
    LCD_WriteData8(0x2C);
    LCD_WriteData8(0x32);

    /* Negative Voltage Gamma Control */
    LCD_Write_Cmd(0xE1);
    LCD_WriteData8(0xD0);
    LCD_WriteData8(0x0A);
    LCD_WriteData8(0x0E);
    LCD_WriteData8(0x0B);
    LCD_WriteData8(0x09);
    LCD_WriteData8(0x25);
    LCD_WriteData8(0x29);
    LCD_WriteData8(0x3A);
    LCD_WriteData8(0x3F);
    LCD_WriteData8(0x27);
    LCD_WriteData8(0x13);
    LCD_WriteData8(0x12);
    LCD_WriteData8(0x2C);
    LCD_WriteData8(0x32);

    /* Display Inversion On */
    LCD_Write_Cmd(0x21);

    LCD_Write_Cmd(0x11);
    delay_ms(120);

    LCD_Write_Cmd(0x29);

    LCD_DisplayOn();
}

void LCD_Address_Set(u16 x1, u16 y1, u16 x2, u16 y2)
{
    if (USE_HORIZONTAL == 1)
    {
        y1 = y1 + 80;
        y2 = y2 + 80;
    }
    else if (USE_HORIZONTAL == 3)
    {
        x1 = x1 + 80;
        x2 = x2 + 80;
    }

    LCD_Write_Cmd(0x2a); //列地址设置
    LCD_WriteData16(x1);
    LCD_WriteData16(x2);
    LCD_Write_Cmd(0x2b); //行地址设置
    LCD_WriteData16(y1);
    LCD_WriteData16(y2);
    LCD_Write_Cmd(0x2C); //储存器写
}

void LCD_Draw_ColorPoint(u16 x, u16 y, u16 color)
{
    LCD_Address_Set(x, y, x, y);
    DC_H;
    CS_L;
    LCD_SPI_Send(color >> 8);
    LCD_SPI_Send(color);
    CS_H;
}

void LCD_Draw_Point(u16 x, u16 y)
{
    LCD_Address_Set(x, y, x, y);
    LCD_WriteData16(POINT_COLOR);
}

void set_point_color(u16 color)
{
    POINT_COLOR = color;
}

void set_back_color(u16 color)
{
    BACK_COLOR = color;
}

void LCD_clear(u16 color)
{
    // LCD_Address_Set(0, 0, LCD_Width, LCD_Height);
    set_point_color(color);
    LCD_Fill(0, 0, LCD_Width, LCD_Height);
}

// 在指定区域田中颜色
void LCD_Fill(u16 xs, u16 ys, u16 xe, u16 ye)
{
    u16 i, j;

    LCD_Address_Set(xs, ys, xe - 1, ye - 1);
    CS_L;
    DC_H;
    for (i = ys; i < ye; i++)
    {
        for (j = xs; j < xe; j++)
        {
            LCD_SPI_Send(POINT_COLOR >> 8);
            LCD_SPI_Send(POINT_COLOR);
        }
    }
    CS_H;
}

void LCD_Fill1(u16 xs, u16 ys, u16 w, u16 h)
{
    LCD_Address_Set(xs, ys, xs + w - 1, ys + h - 1);
    CS_L;
    DC_H;
    for (int i = 0; i < w * h * 2; i++)
    {

        LCD_SPI_Send(POINT_COLOR >> 8);
        LCD_SPI_Send(POINT_COLOR);
    }
    CS_H;
}
/**
 * @description: 进度条
 * @param {u16} xs
 * @param {u16} ys
 * @param {u16} xe
 * @param {u16} ye
 * @param {u16} percent 百分比（0-100）
 * @return {*}
 */
void LCD_Fill_PROGRESS(u16 xs, u16 ys, u16 xe, u16 ye, u16 percent)
{
    if (percent == 0)
    {
        LCD_DrawRectangle(xs, ys, xe, ye);
    }
    u16 x = percent * (xe - xs) / 100;
    LCD_Fill(xs, ys, xs + x, ye);
    LCD_ShowxNum(xe + 5, (ys + ye) / 2 - 16 / 2, percent, 3, 16, 1);
}

//画线
// x1,y1:起点坐标
// x2,y2:终点坐标
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2)
{
    u16 t;
    int xerr = 0, yerr = 0, delta_x, delta_y, distance;
    int incx, incy, uRow, uCol;
    delta_x = x2 - x1; //计算坐标增量
    delta_y = y2 - y1;
    uRow = x1;
    uCol = y1;
    if (delta_x > 0)
        incx = 1; //设置单步方向
    else if (delta_x == 0)
        incx = 0; //垂直线
    else
    {
        incx = -1;
        delta_x = -delta_x;
    }
    if (delta_y > 0)
        incy = 1;
    else if (delta_y == 0)
        incy = 0; //水平线
    else
    {
        incy = -1;
        delta_y = -delta_y;
    }
    if (delta_x > delta_y)
        distance = delta_x; //选取基本增量坐标轴
    else
        distance = delta_y;
    for (t = 0; t <= distance + 1; t++) //画线输出
    {
        LCD_Draw_Point(uRow, uCol); //画点
        xerr += delta_x;
        yerr += delta_y;
        if (xerr > distance)
        {
            xerr -= distance;
            uRow += incx;
        }
        if (yerr > distance)
        {
            yerr -= distance;
            uCol += incy;
        }
    }
}

//画矩形
//(x1,y1),(x2,y2):矩形的对角坐标
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2)
{
    LCD_DrawLine(x1, y1, x2, y1);
    LCD_DrawLine(x1, y1, x1, y2);
    LCD_DrawLine(x1, y2, x2, y2);
    LCD_DrawLine(x2, y1, x2, y2);
}

void LCD_Draw_Circle(u16 x0, u16 y0, u8 r)
{
    int a, b;
    int di;
    a = 0;
    b = r;
    di = 3 - (r << 1);

    while (a <= b)
    {
        LCD_Draw_Point(x0 - b, y0 - a);
        LCD_Draw_Point(x0 + b, y0 - a);
        LCD_Draw_Point(x0 - a, y0 + b);
        LCD_Draw_Point(x0 - b, y0 - a);
        LCD_Draw_Point(x0 - a, y0 - b);
        LCD_Draw_Point(x0 + b, y0 + a);
        LCD_Draw_Point(x0 + a, y0 - b);
        LCD_Draw_Point(x0 + a, y0 + b);
        LCD_Draw_Point(x0 - b, y0 + a);
        a++;

        if (di < 0)
            di += 4 * a + 6;
        else
        {
            di += 10 + 4 * (a - b);
            b--;
        }

        LCD_Draw_Point(x0 + a, y0 + b);
    }
}

/**
 * @brief	显示一个ASCII码字符
 *
 * @param   x,y		显示起始坐标
 * @param   chr		需要显示的字符
 * @param   size	字体大小(支持16/24/32号字体)
 *
 * @return  void
 */
void LCD_ShowChar(u16 x, u16 y, char chr, u8 size)
{
    u8 temp, t1, t;
    u8 csize; //得到字体一个字符对应点阵集所占的字节数
    u16 colortemp;
    u8 sta;

    chr = chr - ' '; //得到偏移后的值（ASCII字库是从空格开始取模，所以-' '就是对应字符的字库）

    if ((x > (LCD_Width - size / 2)) || (y > (LCD_Height - size)))
        return;

    LCD_Address_Set(x, y, x + size / 2 - 1, y + size - 1); //(x,y,x+8-1,y+16-1)

    if ((size == 16) || (size == 32)) // 16和32号字体
    {
        csize = (size / 8 + ((size % 8) ? 1 : 0)) * (size / 2);

        for (t = 0; t < csize; t++)
        {
            if (size == 16)
                temp = asc2_1608[chr][t]; //调用1608字体
            else if (size == 32)
                temp = asc2_3216[chr][t]; //调用3216字体
            else
                return; //没有的字库

            for (t1 = 0; t1 < 8; t1++)
            {
                if (temp & 0x80)
                    colortemp = POINT_COLOR;
                else
                    colortemp = BACK_COLOR;

                LCD_WriteData16(colortemp);
                temp <<= 1;
            }
        }
    }

    else if (size == 12) // 12号字体
    {
        csize = (size / 8 + ((size % 8) ? 1 : 0)) * (size / 2);

        for (t = 0; t < csize; t++)
        {
            temp = asc2_1206[chr][t];

            for (t1 = 0; t1 < 6; t1++)
            {
                if (temp & 0x80)
                    colortemp = POINT_COLOR;
                else
                    colortemp = BACK_COLOR;

                LCD_WriteData16(colortemp);
                temp <<= 1;
            }
        }
    }

    else if (size == 24) // 24号字体
    {
        csize = (size * 16) / 8;

        for (t = 0; t < csize; t++)
        {
            temp = asc2_2412[chr][t];

            if (t % 2 == 0)
                sta = 8;
            else
                sta = 4;

            for (t1 = 0; t1 < sta; t1++)
            {
                if (temp & 0x80)
                    colortemp = POINT_COLOR;
                else
                    colortemp = BACK_COLOR;

                LCD_WriteData16(colortemp);
                temp <<= 1;
            }
        }
    }
}

/**
 * @brief	m^n函数
 *
 * @param   m,n		输入参数
 *
 * @return  m^n次方
 */
u32 LCD_Pow(u8 m, u8 n)
{
    u32 result = 1;

    while (n--) result *= m;

    return result;
}

/**
 * @brief	显示数字,高位为0不显示
 *
 * @param   x,y		起点坐标
 * @param   num		需要显示的数字,数字范围(0~4294967295)
 * @param   len		需要显示的位数
 * @param   size	字体大小
 *
 * @return  void
 */
void LCD_ShowNum(u16 x, u16 y, u32 num, u8 len, u8 size)
{
    u8 t, temp;
    u8 enshow = 0;

    for (t = 0; t < len; t++)
    {
        temp = (num / LCD_Pow(10, len - t - 1)) % 10;

        if (enshow == 0 && t < (len - 1))
        {
            if (temp == 0)
            {
                LCD_ShowChar(x + (size / 2) * t, y, ' ', size);
                continue;
            }

            else
                enshow = 1;
        }

        LCD_ShowChar(x + (size / 2) * t, y, temp + '0', size);
    }
}

/**
 * @brief	显示数字,高位为0,可以控制显示为0还是不显示
 *
 * @param   x,y		起点坐标
 * @param   num		需要显示的数字,数字范围(0~999999999)
 * @param   len		需要显示的位数
 * @param   size	字体大小
 * @param   mode	1:高位显示0		0:高位不显示
 *
 * @return  void
 */
void LCD_ShowxNum(u16 x, u16 y, u32 num, u8 len, u8 size, u8 mode)
{
    u8 t, temp;
    u8 enshow = 0;

    for (t = 0; t < len; t++)
    {
        temp = (num / LCD_Pow(10, len - t - 1)) % 10;

        if (enshow == 0 && t < (len - 1))
        {
            if (temp == 0)
            {
                if (mode)
                    LCD_ShowChar(x + (size / 2) * t, y, '0', size);
                else
                    LCD_ShowChar(x + (size / 2) * t, y, ' ', size);

                continue;
            }

            else
                enshow = 1;
        }

        LCD_ShowChar(x + (size / 2) * t, y, temp + '0', size);
    }
}

/**
 * @brief	显示字符串
 *
 * @param   x,y		起点坐标
 * @param   width	字符显示区域宽度
 * @param   height	字符显示区域高度
 * @param   size	字体大小
 * @param   p		字符串起始地址
 *
 * @return  void
 */
void LCD_ShowString(u16 x, u16 y, u16 width, u16 height, u8 size, char *p)
{
    u8 x0 = x;
    width += x;
    height += y;

    while ((*p <= '~') && (*p >= ' ')) //判断是不是非法字符!
    {
        if (x >= width)
        {
            x = x0;
            y += size;
        }

        if (y >= height)
            break; //退出

        LCD_ShowChar(x, y, *p, size);
        x += size / 2;
        p++;
    }
}

/**
 * @brief	显示图片
 *
 * @remark	Image2Lcd取模方式：	C语言数据/水平扫描/16位真彩色(RGB565)/高位在前		其他的不要选
 *
 * @param   x,y		起点坐标
 * @param   width	图片宽度
 * @param   height	图片高度
 * @param   p		图片缓存数据起始地址
 *
 * @return  void
 */
void LCD_Show_Image(u16 x, u16 y, u16 width, u16 height, const u8 *p)
{
    if (x + width > LCD_Width || y + height > LCD_Height)
    {
        return;
    }

    LCD_Address_Set(x, y, x + width - 1, y + height - 1);

    DC_H;

    LCD_WriteData((u8 *)p, width * height * 2);
}
