/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2022-11-14 11:11:27
 * @LastEditors: MichaelHu
 * @LastEditTime: 2022-11-24 09:19:17
 */
#ifndef _ST7789_H_
#define _ST7789_H_

#include "sys.h"

#define HARDWARE_SPI

#define DC_PORT GPIOA, GPIO_Pins_2
#define CS_PORT GPIOA, GPIO_Pins_3
#define RST_PORT GPIOA, GPIO_Pins_4
#define LCD_LED GPIOB, GPIO_Pins_1

#define CS_H GPIO_SetBits(CS_PORT);
#define CS_L GPIO_ResetBits(CS_PORT);
#define DC_H GPIO_SetBits(DC_PORT);
#define DC_L GPIO_ResetBits(DC_PORT);
#define RST_H GPIO_SetBits(RST_PORT);
#define RST_L GPIO_ResetBits(RST_PORT);
#define LCD_LED_ON GPIO_SetBits(LCD_LED);
#define LCD_LED_OFF GPIO_ResetBits(LCD_LED);

#ifndef HARDWARE_SPI
#define SCL_PORT GPIOA, GPIO_Pins_5
#define SDA_PORT GPIOA, GPIO_Pins_7
#define SDA_H GPIO_SetBits(SDA_PORT);
#define SDA_L GPIO_ResetBits(SDA_PORT);
#define SCLK_H GPIO_SetBits(SCL_PORT);
#define SCLK_L GPIO_ResetBits(SCL_PORT);
#endif

#define USE_HORIZONTAL 3

#if (USE_HORIZONTAL == 0) || (USE_HORIZONTAL == 1)
#define LCD_Width 240
#define LCD_Height 320
#elif (USE_HORIZONTAL == 2) || (USE_HORIZONTAL == 3)
#define LCD_Width 320
#define LCD_Height 240
#endif

////������ɫ
#define WHITE 0xFFFF
#define BLACK 0x0000
#define BLUE 0x001F
#define BRED 0XF81F
#define GRED 0XFFE0
#define GBLUE 0X07FF
#define RED 0xF800
#define MAGENTA 0xF81F
#define GREEN 0x07E0
#define CYAN 0x7FFF
#define YELLOW 0xFFE0
#define BROWN 0XBC40 //��ɫ
#define BRRED 0XFC07 //�غ�ɫ
#define GRAY 0X8430  //��ɫ

void LCD_Draw_ColorPoint(u16 x, u16 y, u16 color);
void set_point_color(u16 color);
void set_back_color(u16 color);
void LCD_Fill(u16 xs, u16 ys, u16 xe, u16 ye);
void LCD_Fill_PROGRESS(u16 xs, u16 ys, u16 xe, u16 ye, u16 percent);
void LCD_clear(u16 color);
u32 LCD_Pow(u8 m, u8 n);
void LCD_Address_Set(u16 x1, u16 y1, u16 x2, u16 y2);
void LCD_WriteData16(const u16 da);
void LCD_WriteData(u8 *data, u32 len);
void LCD_ShowChar(u16 x, u16 y, char chr, u8 size);
void LCD_ShowxNum(u16 x, u16 y, u32 num, u8 len, u8 size, u8 mode);
void LCD_ShowString(u16 x, u16 y, u16 width, u16 height, u8 size, char *p);
void LCD_Show_Image(u16 x, u16 y, u16 width, u16 height, const u8 *p);
void LCD_Show_Image_inflash(u32 addr);
void LCD_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2);
void LCD_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2);
void LCD_Init(void);
void LCD_Draw_Circle(u16 x0, u16 y0, u8 r);
void LCD_Show_Image_From_Flash(u16 num);

#endif
