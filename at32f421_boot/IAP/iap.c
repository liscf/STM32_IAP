/*
 * @Descripttion: 
 * @version: 
 * @Author: MichaelHu
 * @Date: 2022-11-16 10:07:43
 * @LastEditors: MichaelHu
 * @LastEditTime: 2022-11-16 11:04:59
 */
#include "iap.h"
#include "delay.h"
#include "flash.h"
#include "sys.h"
#include "usart.h"

iapfun jump2app;
u16 iapbuf[1024];
//appxaddr:应用程序的起始地址
//appbuf:应用程序CODE.
//appsize:应用程序大小(字节).
void iap_write_appbin(u32 appxaddr, u8 *appbuf, u32 appsize)
{
    u16 t;
    u16 i = 0;
    u16 temp;
    u32 fwaddr = appxaddr; //当前写入的地址
    u8 *dfu = appbuf;
    for (t = 0; t < appsize; t += 2)
    {
        temp = (u16)dfu[1] << 8;
        temp += (u16)dfu[0];
        dfu += 2; //偏移2个字节
        iapbuf[i++] = temp;
        if (i == 512)
        {
            i = 0;
            FLASH_Write(fwaddr, iapbuf, 512);
            fwaddr += 1024; //偏移2048  16=2*8.所以要乘以2.
        }
    }
    if (i)
        FLASH_Write(fwaddr, iapbuf, i); //将最后的一些内容字节写进去.
}

//跳转到应用程序段
//appxaddr:用户代码起始地址.
void iap_load_app(u32 appxaddr)
{
    if (((*(vu32 *)appxaddr) & 0x2FFE0000) == 0x20000000) //检查栈顶地址是否合法.
    {
        /*跳转前复位串口和定时器*/
        //__disable_irq();
        USART_Reset(USART1);
        SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
		DMA_Reset(uartDMA_CH);
        TMR_Reset(TMR3);
        jump2app = (iapfun) * (vu32 *)(appxaddr + 4); //用户代码区第二个字为程序开始地址(复位地址)
        MSR_MSP(*(vu32 *)appxaddr);                   //初始化APP堆栈指针(用户代码区的第一个字用于存放栈顶地址)
        jump2app();                                   //跳转到APP.
    }
}
