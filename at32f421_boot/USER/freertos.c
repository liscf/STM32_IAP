#include "FreeRTOS.h"
#include "at32f4xx.h"
#include "delay.h"
#include "easyflash.h"
#include "sfud.h"
#include "st7789.h"
#include "sys.h"
#include "task.h"
#include "usart.h"

#define MAIN_TASK_PRIO 2
#define MAIN_STK_SIZE 512
TaskHandle_t MAIN_Task_Handler;
void MAIN_task(void *pvParameters);

//TaskHandle_t USART_Task_Handler;
//void usart_task(void *pvParameters);

void MX_FREERTOS_Init(void)
{
    xTaskCreate((TaskFunction_t)MAIN_task,
                (const char *)"MAIN_task",
                (uint16_t)MAIN_STK_SIZE,
                (void *)NULL,
                (UBaseType_t)MAIN_TASK_PRIO,
                (TaskHandle_t *)&MAIN_Task_Handler);

//    xTaskCreate((TaskFunction_t)usart_task,
//                (const char *)"usart_task",
//                (uint16_t)512,
//                (void *)NULL,
//                (UBaseType_t)4,
//                (TaskHandle_t *)&USART_Task_Handler);
    vTaskStartScheduler(); //开启任务调度
}

void usart_task(void *pvParameters)
{
    Process_RES_TypeDef res;
    u8 *buf;
    u8 cmd;
    u16 len;
    buf = pvPortMalloc(1024);

    // usart_dma_init(115200);
    printf("%d\r\n", xPortGetFreeHeapSize());
    for (;;)
    {
        res = data_Process_onebuf(uartFIFO, &cmd, buf, &len);
        if (res == OK)
        {
            switch (cmd)
            {
            case 1:
                printf("ok\r\n");
                break;
            case 2:
                send_Serial_CMD(USART1, 12);
                break;
            default:
                break;
            }
        }
        // data_analysis();
        vTaskDelay(5);
    }
}

void MAIN_task(void *pvParameters)
{

    for (;;)
    {
        vTaskDelay(100);
    }
}
