/*
 * @Descripttion:
 * @version:
 * @Author: MichaelHu
 * @Date: 2021-08-19 11:21:31
 * @LastEditors: MichaelHu
 * @LastEditTime: 2023-01-29 15:35:48
 */
package main

import (
	"fmt"
	"main/my_interface"
	"os"

	"gopkg.in/ini.v1"
)

func main() {
	//读取config.ini  portName 串口号，baud 波特率， bootPath bin文件目录
	//bootPath没有则默认读取当前目录project.bin
	cfg, err := ini.Load("config.ini")
	if err != nil {
		fmt.Printf("Fail to read file: %v", err)
		os.Exit(1)
	}
	portName := cfg.Section("").Key("portName").String()
	baud, _ := cfg.Section("").Key("baud").Int()
	bootPath := cfg.Section("").Key("bootPath").String()
	fmt.Println("start")
	my_interface.SerialStart(portName, baud, bootPath)
}
