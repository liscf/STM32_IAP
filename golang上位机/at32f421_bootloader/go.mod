module main

go 1.15

require (
	github.com/fogleman/gg v1.3.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/image v0.0.0-20220617043117-41969df76e82
	gopkg.in/ini.v1 v1.67.0
)
