#include "sys.h"
#include "delay.h"
#include "usart.h"
#include "led.h"
 

int main(void)
{	
	SCB->VTOR = FLASH_BASE | 0x8000;
	delay_init();	     
	LED_Init();		  
	while(1)
	{
		LED0=0;
    	delay_ms(300);	
		LED0=1;
		delay_ms(300);	
	}
}

