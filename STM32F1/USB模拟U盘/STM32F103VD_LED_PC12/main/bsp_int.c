/************************** ************************** *************************
* File Name          : bsp_int.c
* Author             : 
* Version            : V2.1
* Date               : 2010.12.3
* Description        : QQ:260869626
********************************************************************************
* 
* 
* 
*
* 
*******************************************************************************/
/* Includes ------------------------------------------------------------------*/
#include "stm32f10x.h"
#include "bsp_int.h"
#include "platform_config.h"

/* 头字节包序号，末两字节CRC */
u8 RX_BUFF[1024];



/*******************************************************************************
* Function Name  : 
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void UART1_SendByte(u16 Data)
{ 
   while (!(USART1->SR & USART_FLAG_TXE));
   USART1->DR = (Data & (u16)0x01FF);
   while(USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET) 
  { 
  } 
}

/*******************************************************************************
* Function Name  : 
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void UART1Write(u8* data,u16 len)
{
    u16 i;

    USART_ClearFlag(USART1, USART_FLAG_TXE);
    for (i=0; i<len; i++)
    {
            UART1_SendByte(data[i]);
    }    			
}				  

/*******************************************************************************
* Function Name  : 
* Description    : 中断配置
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void NVIC_Configuration(void)
{
  	NVIC_InitTypeDef NVIC_InitStructure;
	#ifdef  VECT_TAB_RAM  
 	 NVIC_SetVectorTable(NVIC_VectTab_RAM, 0x0); 
	#else  
	  NVIC_SetVectorTable(NVIC_VectTab_FLASH, 0x0000);   
	#endif
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);  
	/* USART1 */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* 启动时钟安全系统CSS */
	RCC_ClockSecuritySystemCmd(ENABLE); 
}

/*******************************************************************************
* Function Name  : 
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void GPIO_Configuration(void)
{						    
      GPIO_InitTypeDef  gpio_init;


      RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
           
      gpio_init.GPIO_Pin   = GPIO_Pin_10;     //LED
      gpio_init.GPIO_Speed = GPIO_Speed_50MHz;
      gpio_init.GPIO_Mode  = GPIO_Mode_Out_PP;

      GPIO_Init(GPIOB, &gpio_init);	
      
      
      RCC_APB2PeriphClockCmd(BOOT_GPIO_CLK, ENABLE);
      
      gpio_init.GPIO_Pin   = BOOT_DISCONNECT_PIN;//boot usb 启动引脚，这个引脚检测obd口的电压，如果obd接了电源则直接启动app，否则进入boot升级模式
      gpio_init.GPIO_Speed = GPIO_Speed_50MHz;
      gpio_init.GPIO_Mode  = GPIO_Mode_IPU;
      
      GPIO_Init(BOOT_DISCONNECT, &gpio_init);
	
	
	    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
      
      gpio_init.GPIO_Pin   = GPIO_Pin_12;//boot usb 启动引脚，这个引脚检测obd口的电压，如果obd接了电源则直接启动app，否则进入boot升级模式
      gpio_init.GPIO_Speed = GPIO_Speed_50MHz;
      gpio_init.GPIO_Mode  = GPIO_Mode_Out_PP;
      
      GPIO_Init(GPIOC, &gpio_init);
}	

/*******************************************************************************
* Function Name  : 
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void USART1_config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	USART_InitTypeDef USART_InitStructure;
	/*开usart时钟*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);  
	
	/* 配置USART1 Rx (PA.10) 为输入 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	/* 配置 USART1 Tx (PA.09) 为输出 */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	USART_InitStructure.USART_BaudRate = 115200;
	USART_InitStructure.USART_WordLength = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits = USART_StopBits_1;
	USART_InitStructure.USART_Parity = USART_Parity_No ;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART1,&USART_InitStructure); 
		
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART1, ENABLE);  

} 

/*******************************************************************************
* Function Name  : 
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void Usart_DMA_config(void)
{

  	DMA_InitTypeDef DMA_InitStructure;
        /*开DMA时钟*/
  	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);		
  	/* DMA1 channel5 接收 */
  	DMA_Cmd(DMA1_Channel5, DISABLE);
  	DMA_DeInit(DMA1_Channel5);
	/* USART1_DR_Base串口接收寄存器地址 */
  	DMA_InitStructure.DMA_PeripheralBaseAddr = 0x40013804;           
  	DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)&RX_BUFF;
  	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
	/* 此值会随接收长度改变 */
  	DMA_InitStructure.DMA_BufferSize = 5;
	/* 外设地址递增禁止 */					            
  	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; 
	/* 内存递增使能 */ 
  	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;		
	/* 接收数据宽度一字节 */	
  	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	/* 内存数据宽度一字节 */
  	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;	
	/* 单次模式 */
  	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;   
	/* 优先级 */                 
  	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;	
	/* 内存到内存传输禁止 */			
  	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
	/* 初始化DMA5 */						
  	DMA_Init(DMA1_Channel5, &DMA_InitStructure); 
	/* 未接收到数据头前禁止DMA接收 */						
  	//USART_DMACmd(USART1, USART_DMAReq_Rx, ENABLE);					
  	DMA_Cmd(DMA1_Channel5, ENABLE);
}

/*******************************************************************************
* Function Name  : 
* Description    : 定时器2配置
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void TIM2_Configuration(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;

	RCC_APB1PeriphClockCmd( RCC_APB1Periph_TIM2,ENABLE );  

	TIM_TimeBaseStructure.TIM_Period = 65535;
  	TIM_TimeBaseStructure.TIM_Prescaler = 0;
  	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
  	TIM_PrescalerConfig(TIM2, 4, TIM_PSCReloadMode_Immediate);
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing;
  	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	/* TIM2 7.2M / 49152 =  146.48  Hz	*/
  	TIM_OCInitStructure.TIM_Pulse = 49152; 
  	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  	TIM_OC1Init(TIM2, &TIM_OCInitStructure);
  	TIM_OC1PreloadConfig(TIM2, TIM_OCPreload_Disable);
 	TIM_ITConfig(TIM2, TIM_IT_CC1 , ENABLE);
  	TIM_Cmd(TIM2, ENABLE);
}

/*******************************************************************************
* Function Name  : 
* Description    : 定时器3初始化、用于串口接收超时判断
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void TIM3_Configuration(void)
{
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;

        RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3,ENABLE ); 
		
	TIM_TimeBaseStructure.TIM_Period = 65535;
  	TIM_TimeBaseStructure.TIM_Prescaler = 0;
  	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);
  	TIM_PrescalerConfig(TIM3, 4, TIM_PSCReloadMode_Immediate);
        TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Timing;
  	TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
	/*  TIM2 7.2M / 49152 =  146.48  Hz */
  	TIM_OCInitStructure.TIM_Pulse = 49152;
  	TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;
  	TIM_OC1Init(TIM3, &TIM_OCInitStructure);
  	TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Disable);
        TIM_ITConfig(TIM3, TIM_IT_CC1 , ENABLE);
	/* 平时关闭 */
        TIM_Cmd(TIM3, DISABLE); 
}

/*******************************************************************************
* Function Name  : 
* Description    : 
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void INIT(void)
{
	 SystemInit();
	 RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOA 
                              |RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC
                              |RCC_APB2Periph_GPIOD | RCC_APB2Periph_AFIO, ENABLE );  
                                                                                                                                                    						                                                    
	 NVIC_Configuration();
	 GPIO_Configuration();
	 USART1_config();
	 Usart_DMA_config();
}


/************************* *************************************END OF FILE****/


