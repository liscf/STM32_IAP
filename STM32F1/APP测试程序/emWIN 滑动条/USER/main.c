/**********************************************************
                   STemWin 滑动条实验
                   作者：胡友刚
                   qq：2586778195
                   电话：18626183405
                   邮箱：hyghyg126@126.com

**********************************************************/

#include "sys.h"
#include "stm32f10x.h"
#include "delay.h"
#include "usart.h"
#include "ILI9481_TFT.h"
#include "GUI.h"	
#include "GUIDEMO.h"
#include "key.h"
#include "led.h"
#include "touch.h"
#include "24cxx.h"
#include "DIALOG.h"
#include "pwm_output.h"

/**********************************************************
                           主函数
**********************************************************/
int main(void) 
{
	SCB->VTOR = FLASH_BASE | 0x3000;
	uart_init(9600);	//初始化串口，设置波特率为9600bps
	TP_Init(); 
	LED_Init();
	SysTick_Config(SystemCoreClock / 1000);
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_CRC, ENABLE);	//开启CRC时钟
	WM_SetCreateFlags(WM_CF_MEMDEV);
	GUI_Init();
	ILI9481_TFT_Scan_Dir(L2R_U2D);
   
	PROGBAR_SetDefaultSkin(PROGBAR_SKIN_FLEX);
	FRAMEWIN_SetDefaultSkin(FRAMEWIN_SKIN_FLEX);
	PROGBAR_SetDefaultSkin(PROGBAR_SKIN_FLEX);
	BUTTON_SetDefaultSkin(BUTTON_SKIN_FLEX);
	CHECKBOX_SetDefaultSkin(CHECKBOX_SKIN_FLEX);
	SCROLLBAR_SetDefaultSkin(SCROLLBAR_SKIN_FLEX);
	SLIDER_SetDefaultSkin(SLIDER_SKIN_FLEX);
	HEADER_SetDefaultSkin(HEADER_SKIN_FLEX);
    SPINBOX_SetDefaultSkin(SPINBOX_SKIN_FLEX);
	
	MainTask();
}

