#ifndef __ILI9481_TFT_H
#define __ILI9481_TFT_H		
#include "sys.h"	 
#include "stdlib.h"
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//ALIENTEK战舰STM32开发板
//2.4/2.8寸/3.5寸 TFT液晶驱动	  
//支持驱动IC型号包括:ILI9341/ILI9325/RM68042/RM68021/ILI9320/ILI9328/LGDP4531/LGDP4535/SPFD5408/SSD1289/1505/B505/C505等	    
//正点原子@ALIENTEK
//技术论坛:www.openedv.com
//修改日期:2012/9/30
//版本：V2.1
//版权所有，盗版必究。
//Copyright(C) 广州市星翼电子科技有限公司 2009-2019
//All rights reserved	
//********************************************************************************
//V1.2修改说明
//支持了SPFD5408的驱动,另外把液晶ID直接打印成HEX格式.方便查看ILI9481_TFT驱动IC.
//V1.3
//加入了快速IO的支持
//修改了背光控制的极性（适用于V1.8及以后的开发板版本）
//对于1.8版本之前(不包括1.8)的液晶模块,请修改ILI9481_TFT_Init函数的ILI9481_TFT_LED=1;为ILI9481_TFT_LED=1;
//V1.4
//修改了ILI9481_TFT_ShowChar函数，使用画点功能画字符。
//加入了横竖屏显示的支持
//V1.5 20110730
//1,修改了B505液晶读颜色有误的bug.
//2,修改了快速IO及横竖屏的设置方式.
//V1.6 20111116
//1,加入对LGDP4535液晶的驱动支持
//V1.7 20120713
//1,增加ILI9481_TFT_RD_DATA函数
//2,增加对ILI9341的支持
//3,增加ILI9325的独立驱动代码
//4,增加ILI9481_TFT_Scan_Dir函数(慎重使用)	  
//6,另外修改了部分原来的函数,以适应9341的操作
//V1.8 20120905
//1,加入ILI9481_TFT重要参数设置结构体ILI9481_TFTdev
//2,加入ILI9481_TFT_Display_Dir函数,支持在线横竖屏切换
//V1.9 20120911
//1,新增RM68042驱动（ID:6804），但是6804不支持横屏显示！！原因：改变扫描方式，
//导致6804坐标设置失效，试过很多方法都不行，暂时无解。
//V2.0 20120924
//在不硬件复位的情况下,ILI9341的ID读取会被误读成9300,修改ILI9481_TFT_Init,将无法识别
//的情况（读到ID为9300/非法ID）,强制指定驱动IC为ILI9341，执行9341的初始化。
//V2.1 20120930
//修正ILI9325读颜色的bug。
//////////////////////////////////////////////////////////////////////////////////	 
 
  
//ILI9481_TFT重要参数集
typedef struct  
{										    
	u16 width;			//ILI9481_TFT 宽度
	u16 height;			//ILI9481_TFT 高度
	u16 id;				//ILI9481_TFT ID
	u8  dir;			//横屏还是竖屏控制：0，竖屏；1，横屏。	
	u8	wramcmd;		//开始写gram指令
	u8  setxcmd;		//设置x坐标指令
	u8  setycmd;		//设置y坐标指令	 
}_ILI9481_TFT_dev; 	  

//ILI9481_TFT参数
extern _ILI9481_TFT_dev ILI9481_TFTdev;	//管理ILI9481_TFT重要参数
//ILI9481_TFT的画笔颜色和背景色	   
extern u16  POINT_COLOR;//默认红色    
extern u16  BACK_COLOR; //背景颜色.默认为白色


//////////////////////////////////////////////////////////////////////////////////	 
//-----------------ILI9481_TFT端口定义---------------- 
#define	ILI9481_TFT_LED PBout(0) //ILI9481_TFT背光    		 PB0 	    
//ILI9481_TFT地址结构体
typedef struct
{
	u16 ILI9481_TFT_REG;
	u16 ILI9481_TFT_RAM;
} ILI9481_TFT_TypeDef;
//使用NOR/SRAM的 Bank1.sector4,地址位HADDR[27,26]=11 A10作为数据命令区分线 
//注意设置时STM32内部会右移一位对其! 111110=0X3E			    
#define ILI9481_TFT_BASE        ((u32)(0x60000000 | 0x0001FFFE))
#define ILI9481_TFT             ((ILI9481_TFT_TypeDef *) ILI9481_TFT_BASE)
//////////////////////////////////////////////////////////////////////////////////
	 
//扫描方向定义
#define L2R_U2D  0 //从左到右,从上到下
#define L2R_D2U  1 //从左到右,从下到上
#define R2L_U2D  2 //从右到左,从上到下
#define R2L_D2U  3 //从右到左,从下到上

#define U2D_L2R  4 //从上到下,从左到右
#define U2D_R2L  5 //从上到下,从右到左
#define D2U_L2R  6 //从下到上,从左到右
#define D2U_R2L  7 //从下到上,从右到左	 

#define DFT_SCAN_DIR  L2R_U2D  //默认的扫描方向

//画笔颜色
#define WHITE         	 0xFFFF
#define BLACK         	 0x0000	  
#define BLUE         	 0x001F  
#define BRED             0XF81F
#define GRED 			 0XFFE0
#define GBLUE			 0X07FF
#define RED           	 0xF800
#define MAGENTA       	 0xF81F
#define GREEN         	 0x07E0
#define CYAN          	 0x7FFF
#define YELLOW        	 0xFFE0
#define BROWN 			 0XBC40 //棕色
#define BRRED 			 0XFC07 //棕红色
#define GRAY  			 0X8430 //灰色
//GUI颜色

#define DARKBLUE      	 0X01CF	//深蓝色
#define LIGHTBLUE      	 0X7D7C	//浅蓝色  
#define GRAYBLUE       	 0X5458 //灰蓝色
//以上三色为PANEL的颜色 
 
#define LIGHTGREEN     	 0X841F //浅绿色
//#define LIGHTGRAY        0XEF5B //浅灰色(PANNEL)
#define LGRAY 			 0XC618 //浅灰色(PANNEL),窗体背景色

#define LGRAYBLUE        0XA651 //浅灰蓝色(中间层颜色)
#define LBBLUE           0X2B12 //浅棕蓝色(选择条目的反色)
	    															  
void ILI9481_TFT_Init(void);													   	//初始化
void ILI9481_TFT_DisplayOn(void);													//开显示
void ILI9481_TFT_DisplayOff(void);													//关显示
void ILI9481_TFT_Clear(u16 Color);	 												//清屏
void ILI9481_TFT_SetCursor(u16 Xpos, u16 Ypos);										//设置光标
void ILI9481_TFT_DrawPoint(u16 x,u16 y);											//画点
void ILI9481_TFT_Fast_DrawPoint(u16 x,u16 y,u16 color);								//快速画点
u16  ILI9481_TFT_ReadPoint(void); 											//读点 
void Draw_Circle(u16 x0,u16 y0,u8 r);										//画圆
void ILI9481_TFT_DrawLine(u16 x1, u16 y1, u16 x2, u16 y2);							//画线
void ILI9481_TFT_DrawRectangle(u16 x1, u16 y1, u16 x2, u16 y2);		   				//画矩形
void ILI9481_TFT_Fill(u16 sx,u16 sy,u16 ex,u16 ey,u16 color);		   				//填充单色
void ILI9481_TFT_Color_Fill(u16 sx,u16 sy,u16 ex,u16 ey,u16 *color);				//填充指定颜色
void ILI9481_TFT_ShowChar(u16 x,u16 y,u8 num,u8 size,u8 mode);						//显示一个字符
void ILI9481_TFT_ShowNum(u16 x,u16 y,u32 num,u8 len,u8 size);  						//显示一个数字
void ILI9481_TFT_ShowxNum(u16 x,u16 y,u32 num,u8 len,u8 size,u8 mode);				//显示 数字
void ILI9481_TFT_ShowString(u16 x,u16 y,u16 width,u16 height,u8 size,u8 *p);		//显示一个字符串,12/16字体
u16 ILI9481_TFT_BGR2RGB(u16 c);

void ILI9481_TFT_WriteReg(u8 ILI9481_TFT_Reg, u16 ILI9481_TFT_RegValue);
u16 ILI9481_TFT_ReadReg(u8 ILI9481_TFT_Reg);
void ILI9481_TFT_WriteRAM_Prepare(void);
void ILI9481_TFT_WriteRAM(u16 RGB_Code);		  
void ILI9481_TFT_Scan_Dir(u8 dir);							//设置屏扫描方向
void ILI9481_TFT_Display_Dir(u8 dir);						//设置屏幕显示方向
 					   																			 
//9320/9325 ILI9481_TFT寄存器  
#define R0             0x00
#define R1             0x01
#define R2             0x02
#define R3             0x03
#define R4             0x04
#define R5             0x05
#define R6             0x06
#define R7             0x07
#define R8             0x08
#define R9             0x09
#define R10            0x0A
#define R12            0x0C
#define R13            0x0D
#define R14            0x0E
#define R15            0x0F
#define R16            0x10
#define R17            0x11
#define R18            0x12
#define R19            0x13
#define R20            0x14
#define R21            0x15
#define R22            0x16
#define R23            0x17
#define R24            0x18
#define R25            0x19
#define R26            0x1A
#define R27            0x1B
#define R28            0x1C
#define R29            0x1D
#define R30            0x1E
#define R31            0x1F
#define R32            0x20
#define R33            0x21
#define R34            0x22
#define R36            0x24
#define R37            0x25
#define R40            0x28
#define R41            0x29
#define R43            0x2B
#define R45            0x2D
#define R48            0x30
#define R49            0x31
#define R50            0x32
#define R51            0x33
#define R52            0x34
#define R53            0x35
#define R54            0x36
#define R55            0x37
#define R56            0x38
#define R57            0x39
#define R59            0x3B
#define R60            0x3C
#define R61            0x3D
#define R62            0x3E
#define R63            0x3F
#define R64            0x40
#define R65            0x41
#define R66            0x42
#define R67            0x43
#define R68            0x44
#define R69            0x45
#define R70            0x46
#define R71            0x47
#define R72            0x48
#define R73            0x49
#define R74            0x4A
#define R75            0x4B
#define R76            0x4C
#define R77            0x4D
#define R78            0x4E
#define R79            0x4F
#define R80            0x50
#define R81            0x51
#define R82            0x52
#define R83            0x53
#define R96            0x60
#define R97            0x61
#define R106           0x6A
#define R118           0x76
#define R128           0x80
#define R129           0x81
#define R130           0x82
#define R131           0x83
#define R132           0x84
#define R133           0x85
#define R134           0x86
#define R135           0x87
#define R136           0x88
#define R137           0x89
#define R139           0x8B
#define R140           0x8C
#define R141           0x8D
#define R143           0x8F
#define R144           0x90
#define R145           0x91
#define R146           0x92
#define R147           0x93
#define R148           0x94
#define R149           0x95
#define R150           0x96
#define R151           0x97
#define R152           0x98
#define R153           0x99
#define R154           0x9A
#define R157           0x9D
#define R192           0xC0
#define R193           0xC1
#define R229           0xE5							  		 
#endif  
	 
	 



