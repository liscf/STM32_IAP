#include "delay.h"
#include "sys.h"
#include "usart.h"
#include "stmflash.h"
#include "iap.h"
#include <string.h>
#include <stdlib.h>

/*  单次接收的字节需要时1024的整数倍,由于STM32的Flash块最小由1K组成
    如果换成其他单片机可以根据情况修改。
    1.采用ZET系列的单片机512Kflash，串口缓存55K，如果使用容量小的单片机需要修改 USART_REC_LEN 以及 下位机和上位机的ByteOne大小。
	2.采用了简单的CRC8校验，这里可以根据自己需要修改。
	3.未采用加密方式。
	4.支持大文件更新。
	
	.作者联系方式：hyghyg126@126.com，可随便使用无需注明。
*/
#define ByteOne		(int)(10*1024) 	//单次接收字节大小
u8 CrcRead[20] = {0};	//读取CRC的数组
u8 CrcCount[20] = {0};	//CRC计算的值

void USART1_IRQHandler(void)
{
    u8 res;

    if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)//接收到数据
    {
        res = USART_ReceiveData(USART1);

        if(USART_RX_CNT < USART_REC_LEN)
        {
            USART_RX_BUF[USART_RX_CNT] = res;
            USART_RX_CNT++;
        }
    }
}

u8 CRC8(u8 buffer[], int startlength, int length)
{
    u8 crc = 0;

    for(int j = 0; j < length; j++)
    {
        crc ^= buffer[startlength + j];

        for(int i = 0; i < 8; i++)
        {
            if((crc & 0x01) != 0)
            {
                crc >>= 1;
                crc ^= 0x8c;
            }
            else
            {
                crc >>= 1;
            }
        }
    }

    return crc;
}

int main(void)
{
    u16 t;
    static u32 AppLen;	//上位机发送的APP长度，上位机直接发送的数据长度用于验证接收的是否完整
    char *p;
    u8 count = 0;
	
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//设置系统中断优先级分组2
    delay_init();  //初始化延时函数
    uart_init(460800);		//初始化串口波特率为115200
	
    delay_ms(500);		//启动后适当延时用以接收串口数据

	//判断是否接受到上位机的start起始码，接收到回应SETOK。
    while(1)
	{
		if(t % 50 == 0)
		{
			count++;
			printf("begin");
			printf("%06d",ByteOne);
			printf("\r\n");
			if(count >= 3)	//没有接收到上位机起始命令开始执行APP
			{			
				//跳转到APP
				if(((*(vu32*)(FLASH_APP1_ADDR + 4)) & 0xFF000000) == 0x08000000) //判断是否为0X08XXXXXX.
				{
					iap_load_app(FLASH_APP1_ADDR);//执行FLASH APP代码
				}	
			}
		}
		if(strstr(USART_RX_BUF, "\r\n")&&strstr(USART_RX_BUF, "start"))
		{	
			p = strstr(USART_RX_BUF, "start");
			p[12] = 0;
			AppLen = atol(&p[5]);		//获取长度APP长度。

			for(int i = 0; i <= AppLen / ByteOne; i++)
			{
				CrcRead[i] = p[14 + i];
			}
			break;
		}
		t++;
		delay_ms(10);
	}
	
			
	USART_RX_CNT = 0;
	printf("SETOK\r\n");	//回复上位机准备完成。  
    int LenNum = (int)(AppLen / ByteOne);			//计算需要接收多少次
    int LenNum1 = (int)(AppLen / ByteOne);			//LenNum1 LenNum1用于计算写入flash的地址
	
    while(1)
    {
        if(LenNum > 0)
        {
            if(USART_RX_CNT == ByteOne)
            {				
                USART_RX_CNT = 0;
                LenNum--;
                CrcCount[LenNum1 - LenNum - 1]	= CRC8(USART_RX_BUF , 0 , ByteOne);

                iap_write_appbin(FLASH_APP1_ADDR + (LenNum1 - LenNum - 1)*ByteOne, USART_RX_BUF, ByteOne); //更新FLASH代码
                //memset(USART_RX_BUF,0,55*1024);
                //delay_ms(100);
                printf("next\r\n");		//发送同步信息通知上位机发下一帧数据
            }		
        }
        else if(LenNum == 0)	//接收不足50K的数据
        {
            if(USART_RX_CNT == (int)(AppLen % ByteOne))
            {
                CrcCount[LenNum1]	= CRC8(USART_RX_BUF , 0 , USART_RX_CNT);
                iap_write_appbin(FLASH_APP1_ADDR + LenNum1 * ByteOne, USART_RX_BUF, USART_RX_CNT); //更新FLASH代码
                //memset(USART_RX_BUF,0,55*1024);
                USART_RX_CNT = 0;
                delay_ms(100);

				for(int i = 0; i <= AppLen / ByteOne; i++)
				{
					if(CrcRead[i] == CrcCount[i])
					{
						break;
					}
					else
					{
						printf("crcerr\r\n");
						while(1)
						{
							
						}
					} 
				}
				delay_ms(100);
                //更新完成跳转到APP
                if(((*(vu32*)(FLASH_APP1_ADDR + 4)) & 0xFF000000) == 0x08000000) //判断是否为0X08XXXXXX.
                {
                    iap_load_app(FLASH_APP1_ADDR);//执行FLASH APP代码
                }

            }
        }
		//delay_ms(10);
    }
}













